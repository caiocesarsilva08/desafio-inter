package com.inter.desafiointer.factory;

import com.inter.desafiointer.model.entity.InvestmentOrder;
import com.inter.desafiointer.model.entity.InvestmentOrderItem;
import com.inter.desafiointer.model.response.DetailedCompanyInvestmentOrderResponse;

import java.util.ArrayList;
import java.util.List;

public class InvestmentOrderFactory {
    public static InvestmentOrder generateInvestmentOrderMock() {
        return GenericMockDataFactory.of(InvestmentOrder.class).build();
    }

    public static InvestmentOrderItem generateInvestmentOrderItemMock() {
        return GenericMockDataFactory.of(InvestmentOrderItem.class).build();
    }

    public static List<InvestmentOrder> generateInvestmentOrderListMock(Integer listSize) {
        List<InvestmentOrder> investmentOrderList = new ArrayList<>();

        for (int i = 0; i < listSize; i++) {
            investmentOrderList.add(generateInvestmentOrderMock());
        }

        return investmentOrderList;
    }

    public static List<InvestmentOrderItem> generateInvestmentOrderItemListMock(Integer listSize) {
        List<InvestmentOrderItem> investmentOrderItemList = new ArrayList<>();

        for (int i = 0; i < listSize; i++) {
            investmentOrderItemList.add(generateInvestmentOrderItemMock());
        }

        return investmentOrderItemList;
    }

    public static DetailedCompanyInvestmentOrderResponse generateDetailedInvestmentOrderResponseMock() {
        return GenericMockDataFactory.of(DetailedCompanyInvestmentOrderResponse.class).build();
    }

    public static List<DetailedCompanyInvestmentOrderResponse> generateDetailedInvestmentOrderResponseListMock(Integer listSize) {
        List<DetailedCompanyInvestmentOrderResponse> investmentOrderResponses = new ArrayList<>();

        for (int i = 0; i < listSize; i++) {
            investmentOrderResponses.add(generateDetailedInvestmentOrderResponseMock());
        }

        return investmentOrderResponses;
    }
}
