package com.inter.desafiointer.factory;

import uk.co.jemos.podam.api.*;

import java.util.List;

public class GenericMockDataFactory<T> {
    private final Class<T> type;
    private final PodamFactory podamFactory;
    private final AbstractClassInfoStrategy classInfoStrategy;

    private GenericMockDataFactory(Class<T> type) {
        this.type = type;
        podamFactory = new PodamFactoryImpl(new RandomDataProviderStrategyImpl());
        classInfoStrategy = DefaultClassInfoStrategy.getInstance();
    }

    static <T> GenericMockDataFactory<T> of(Class<T> type) {
        return new GenericMockDataFactory<>(type);
    }

    GenericMockDataFactory<T> excludeField(String field) {
        podamFactory.setClassStrategy(classInfoStrategy.addExcludedField(type, field));
        return this;
    }

    T build() {
        return podamFactory.manufacturePojo(type);
    }


    @SuppressWarnings("unchecked")
    public List<T> buildList(int elementSizeOfCollection) {
        podamFactory.getStrategy().setDefaultNumberOfCollectionElements(elementSizeOfCollection);
        return podamFactory.manufacturePojo(List.class, type);
    }
}

