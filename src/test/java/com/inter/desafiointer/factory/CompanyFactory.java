package com.inter.desafiointer.factory;

import com.inter.desafiointer.model.entity.Company;
import com.inter.desafiointer.model.request.CompanyRequest;
import com.inter.desafiointer.model.request.UpdateCompanyStatusRequest;
import com.inter.desafiointer.model.request.UpdateCompanyStockPriceRequest;

import java.util.ArrayList;
import java.util.List;

public class CompanyFactory {
    public static CompanyRequest generateCompanyRequestMock() {
        return GenericMockDataFactory.of(CompanyRequest.class).build();
    }

    public static List<CompanyRequest> generateCompanyRequestListMock(Integer listSize) {
        List<CompanyRequest> companyRequestList = new ArrayList<>();
        for (int i = 0; i < listSize; i++) {
            companyRequestList.add(generateCompanyRequestMock());
        }
        return companyRequestList;
    }

    public static Company generateCompanyMock() {
        return GenericMockDataFactory.of(Company.class).build();
    }

    public static List<Company> generateCompanyListMock(Integer listSize) {
        List<Company> companies = new ArrayList<Company>();
        for (int i = 0; i < listSize; i++) {
            companies.add(generateCompanyMock());
        }
        return companies;
    }

    public static List<Company> generateActiveCompanyListMock(Integer listSize) {
        List<Company> activeCompanies = new ArrayList<Company>();
        for (int i = 0; i < listSize; i++) {
            Company activeCompany = generateCompanyMock();
            activeCompany.setActive(true);
            activeCompanies.add(activeCompany);
        }
        return activeCompanies;
    }

    public static UpdateCompanyStockPriceRequest generateUpdateCompanyStockPriceRequestMock() {
        return GenericMockDataFactory.of(UpdateCompanyStockPriceRequest.class).build();
    }

    public static UpdateCompanyStatusRequest generateUpdateCompanyStatusRequestMock() {
        return GenericMockDataFactory.of(UpdateCompanyStatusRequest.class).build();
    }
}
