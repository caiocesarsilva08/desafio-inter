package com.inter.desafiointer.service;

import com.inter.desafiointer.model.converter.InvestmentOrderConverter;
import com.inter.desafiointer.model.entity.InvestmentOrder;
import com.inter.desafiointer.model.entity.InvestmentOrderItem;
import com.inter.desafiointer.model.response.DetailedCompanyInvestmentOrderResponse;
import com.inter.desafiointer.repository.InvestmentOrderItemRepository;
import com.inter.desafiointer.repository.InvestmentOrderRepository;
import com.inter.desafiointer.service.base.BaseServiceTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static com.inter.desafiointer.factory.InvestmentOrderFactory.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class InvestmentOrderServiceTest extends BaseServiceTest {
    private InvestmentOrderConverter investmentOrderConverter;

    private static final int LIST_SIZE = 4;
    private List<InvestmentOrder> investmentOrderList;
    private List<InvestmentOrderItem> investmentOrderItemList;
    private List<DetailedCompanyInvestmentOrderResponse> detailedCompanyInvestmentOrderResponses;
    private InvestmentOrder investmentOrder;

    @Autowired
    private InvestmentOrderService investmentOrderService;

    @MockBean
    private InvestmentOrderRepository investmentOrderRepository;

    @MockBean
    private InvestmentOrderItemRepository investmentOrderItemRepository;

    @BeforeEach
    void setUp(){
        investmentOrder = generateInvestmentOrderMock();
        investmentOrderList = generateInvestmentOrderListMock(LIST_SIZE);
        investmentOrderItemList = generateInvestmentOrderItemListMock(LIST_SIZE);
        detailedCompanyInvestmentOrderResponses = generateDetailedInvestmentOrderResponseListMock(LIST_SIZE);

        investmentOrderConverter = new InvestmentOrderConverter();
    }

    @Test
    void GetAllDetailedOrders_ShouldSuccess_GivenThat_ThereAreOrdersAndOrderItems() {
        //Arrange
        when(investmentOrderRepository.findAll()).thenReturn(investmentOrderList);
        when(investmentOrderItemRepository.findAll()).thenReturn(investmentOrderItemList);

        //Act
        List<DetailedCompanyInvestmentOrderResponse> response = investmentOrderService.getAllDetailedOrders();

        //Assert
        assertEquals(LIST_SIZE, response.size());
        for (int i = 0; i < response.size(); i++) {
            //Assert for each property
            assertEquals(investmentOrderList.get(i).getCpf(), response.get(i).getCpf());
            assertEquals(investmentOrderList.get(i).getOrderId(), response.get(i).getId());
            assertEquals(investmentOrderList.get(i).getOrderPayment(), response.get(i).getOrderPayment());
            assertEquals(investmentOrderList.get(i).getChangeOrder(), response.get(i).getChangeOrder());
            assertEquals(investmentOrderList.get(i).getOrderTotal(), response.get(i).getOrderTotal());
            for(int j = 0; j < response.get(i).getOrderItems().size(); j++) {
                assertEquals(investmentOrderItemList.get(j).getStockQuantity(), response.get(i).getOrderItems().get(j).getStockQuantity());
                assertEquals(investmentOrderItemList.get(j).getCompany().getId(), response.get(i).getOrderItems().get(j).getCompanyId());
                assertEquals(investmentOrderItemList.get(j).getInvestmentOrder().getOrderId(), response.get(i).getId());
                assertEquals(investmentOrderItemList.get(j).getOrderItemId(), response.get(i).getOrderItems().get(j).getOrderItemId());
            }
        }
        verify(investmentOrderRepository, times(1)).findAll();
        verify(investmentOrderItemRepository, times(1)).findAll();
    }

    @Test
    void GetAllDetailedOrders_ShouldNotSuccess_GivenThat_ThereAreNoOrders() {
        //Arrange
        when(investmentOrderRepository.findAll()).thenReturn(null);

        //Act
        List<DetailedCompanyInvestmentOrderResponse> response = investmentOrderService.getAllDetailedOrders();

        //Assert
        assertEquals(0, response.size());
        verify(investmentOrderRepository, times(1)).findAll();
        verify(investmentOrderItemRepository, times(0)).findAll();
    }

    @Test
    void GetAllDetailedOrders_ShouldNotSuccess_GivenThat_ThereAreNoOrdersItems() {
        //Arrange
        when(investmentOrderRepository.findAll()).thenReturn(investmentOrderList);
        when(investmentOrderItemRepository.findAll()).thenReturn(null);

        //Act
        List<DetailedCompanyInvestmentOrderResponse> response = investmentOrderService.getAllDetailedOrders();

        //Assert
        assertEquals(0, response.size());
        verify(investmentOrderRepository, times(1)).findAll();
        verify(investmentOrderItemRepository, times(1)).findAll();
    }

    @Test
    void CalculateCompanyTotals_Should_CalculateTheCompanyTotalCorrectly() {
        //Arrange

        //Act

        //Assert
    }
}
