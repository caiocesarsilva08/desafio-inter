package com.inter.desafiointer.service;

import com.inter.desafiointer.model.converter.CompanyConverter;
import com.inter.desafiointer.model.dto.CompanyDto;
import com.inter.desafiointer.model.entity.Company;
import com.inter.desafiointer.model.request.CompanyRequest;
import com.inter.desafiointer.model.request.UpdateCompanyStatusRequest;
import com.inter.desafiointer.model.request.UpdateCompanyStockPriceRequest;
import com.inter.desafiointer.model.response.CompanyDeleteResponse;
import com.inter.desafiointer.repository.CompanyRepository;
import com.inter.desafiointer.service.base.BaseServiceTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.inter.desafiointer.factory.CompanyFactory.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CompanyServiceTest extends BaseServiceTest {
    private static final int LIST_SIZE = 4;
    private CompanyRequest companyRequest;
    private List<CompanyRequest> companyRequestList;
    private Company company;
    private List<Company> companyList;
    private List<Company> activeCompanyList;
    private CompanyDto companyDto;
    private CompanyConverter companyConverter;
    private UpdateCompanyStockPriceRequest updateCompanyStockPriceRequest;
    private UpdateCompanyStatusRequest updateCompanyStatusRequest;

    @Autowired
    private CompanyService companyService;

    @MockBean
    private CompanyRepository companyRepository;


    @BeforeEach
    void setUp() {
        companyConverter = new CompanyConverter();
        companyRequest = generateCompanyRequestMock();
        companyRequestList = generateCompanyRequestListMock(LIST_SIZE);
        updateCompanyStockPriceRequest = generateUpdateCompanyStockPriceRequestMock();
        updateCompanyStatusRequest = generateUpdateCompanyStatusRequestMock();
        companyList = generateCompanyListMock(LIST_SIZE);
        activeCompanyList = generateActiveCompanyListMock(LIST_SIZE);
        company = companyConverter.toEntity(companyRequest);
        companyDto = companyConverter.toDto(company);
    }

    @Test
    void CreateCompany_ShouldSucceed_GivenThat_CompanyDoesNotExists() {
        //Arrange
        when(companyRepository.existsBySymbol(companyRequest.getSymbol())).thenReturn(false);
        when(companyRepository.save(company)).thenReturn(company);

        //Act
        final CompanyDto response = companyService.createCompany(companyRequest);

        //Assert
        assertEquals(companyDto, response);
        verify(companyRepository, times(1)).save(company);
    }

    @Test
    void CreateCompany_ShouldNotSucceed_GivenThat_CompanyAlreadyExists() {
        //Arrange
        when(companyRepository.existsBySymbol(companyRequest.getSymbol())).thenReturn(true);

        //Act
        final CompanyDto response = companyService.createCompany(companyRequest);

        //Assert
        assertNull(response);
        verify(companyRepository, times(0)).save(company);
    }

    @Test
    void CreateCompanies_ShouldSucceed_GivenThat_NoneOfTheCompaniesExists() {
        //Arrange
        companyRequestList.forEach(item -> {
            when(companyRepository.existsBySymbol(item.getSymbol())).thenReturn(false);
            when(companyRepository.save(companyConverter.toEntity(item))).thenReturn(companyConverter.toEntity(item));
        });

        //Act
        final List<CompanyDto> response = companyService.createCompanies(companyRequestList);

        //Assert
        assertEquals(LIST_SIZE, response.size());
        for (int i = 0; i < LIST_SIZE; i++) {
            assertEquals(companyRequestList.get(i).getSymbol(), response.get(i).getSymbol());
            assertEquals(companyRequestList.get(i).getActive(), response.get(i).getActive());
            assertEquals(companyRequestList.get(i).getCompanyName(), response.get(i).getCompanyName());
            assertEquals(companyRequestList.get(i).getStockPrice(), response.get(i).getStockPrice());
            verify(companyRepository, times(1)).save(companyConverter.toEntity(companyRequestList.get(i)));
        }
    }

    @Test
    void CreateCompanies_Should_AddOnlyCompaniesThatDoesNotExists() {
        //Arrange
        when(companyRepository.existsBySymbol(companyRequestList.get(0).getSymbol())).thenReturn(true);
        when(companyRepository.save(companyConverter.toEntity(companyRequestList.get(0)))).thenReturn(null);
        for (int i = 1; i < LIST_SIZE; i++) {
            when(companyRepository.existsBySymbol(companyRequestList.get(i).getSymbol())).thenReturn(false);
            when(companyRepository.save(companyConverter.toEntity(companyRequestList.get(i)))).thenReturn(companyConverter.toEntity(companyRequestList.get(i)));
        }

        //Act
        final List<CompanyDto> response = companyService.createCompanies(companyRequestList);

        //Assert
        assertEquals(LIST_SIZE - 1, response.size());
    }

    @Test
    void CreateCompanies_ShouldNotSucceed_GivenThat_AllCompaniesAlreadyExists() {
        //Arrange
        for (int i = 0; i < LIST_SIZE; i++) {
            when(companyRepository.existsBySymbol(companyRequestList.get(i).getSymbol())).thenReturn(true);
            when(companyRepository.save(companyConverter.toEntity(companyRequestList.get(i)))).thenReturn(null);
        }

        //Act
        final List<CompanyDto> response = companyService.createCompanies(companyRequestList);

        //Assert
        assertEquals(0, response.size());
    }

    @Test
    void UpdateCompanyStockPrice_ShouldSucceed_GivenThat_CompanyExists() {
        //Arrange
        final BigDecimal newStockPrice = new BigDecimal("123.00");
        company.setStockPrice(newStockPrice);
        updateCompanyStockPriceRequest.setStockPrice(newStockPrice);
        when(companyRepository.existsBySymbol(updateCompanyStockPriceRequest.getSymbol())).thenReturn(true);
        when(companyRepository.findBySymbol(updateCompanyStockPriceRequest.getSymbol())).thenReturn(company);
        when(companyRepository.updateStockPriceBySymbol(updateCompanyStockPriceRequest.getSymbol(), newStockPrice)).thenReturn(1);

        //Act
        final CompanyDto response = companyService.updateCompanyStockPrice(updateCompanyStockPriceRequest);

        //Assert
        assertEquals(newStockPrice, response.getStockPrice());
        verify(companyRepository, times(1)).updateStockPriceBySymbol(updateCompanyStockPriceRequest.getSymbol(), newStockPrice);
    }

    @Test
    void UpdateCompanyStockPrice_ShouldNotSucceed_GivenThat_CompanyDoesNotExists() {
        //Arrange
        final BigDecimal newStockPrice = new BigDecimal("123.00");
        company.setStockPrice(newStockPrice);
        updateCompanyStockPriceRequest.setStockPrice(newStockPrice);
        when(companyRepository.existsBySymbol(updateCompanyStockPriceRequest.getSymbol())).thenReturn(false);
        when(companyRepository.findBySymbol(updateCompanyStockPriceRequest.getSymbol())).thenReturn(null);

        //Act
        final CompanyDto response = companyService.updateCompanyStockPrice(updateCompanyStockPriceRequest);

        //Assert
        assertNull(response);
        verify(companyRepository, times(0)).updateStockPriceBySymbol(updateCompanyStatusRequest.getSymbol(), newStockPrice);
    }

    @Test
    void UpdateCompanyStatus_ShouldSucceed_GivenThat_CompanyExists() {
        //Arrange
        final Boolean newStatus = false;
        company.setActive(newStatus);
        updateCompanyStatusRequest.setActive(newStatus);
        when(companyRepository.existsBySymbol(updateCompanyStatusRequest.getSymbol())).thenReturn(true);
        when(companyRepository.findBySymbol(updateCompanyStatusRequest.getSymbol())).thenReturn(company);
        when(companyRepository.updateStatusBySymbol(updateCompanyStatusRequest.getSymbol(), newStatus)).thenReturn(1);

        //Act
        final CompanyDto response = companyService.updateCompanyStatus(updateCompanyStatusRequest);

        //Assert
        assertEquals(newStatus, response.getActive());
        verify(companyRepository, times(1)).updateStatusBySymbol(updateCompanyStatusRequest.getSymbol(), newStatus);
    }

    @Test
    void UpdateCompanyStatus_ShouldNotSucceed_GivenThat_CompanyDoesNotExists() {
        //Arrange
        final Boolean newStatus = false;
        company.setActive(newStatus);
        updateCompanyStatusRequest.setActive(newStatus);
        when(companyRepository.existsBySymbol(updateCompanyStatusRequest.getSymbol())).thenReturn(false);
        when(companyRepository.findBySymbol(updateCompanyStatusRequest.getSymbol())).thenReturn(null);

        //Act
        final CompanyDto response = companyService.updateCompanyStatus(updateCompanyStatusRequest);

        //Assert
        assertNull(response);
        verify(companyRepository, times(0)).updateStatusBySymbol(updateCompanyStatusRequest.getSymbol(), newStatus);
    }

    @Test
    void DeleteCompany_ShouldSucceed_GivenThat_CompanyExists() {
        //Arrange
        when(companyRepository.existsBySymbol(company.getSymbol())).thenReturn(true);

        //Act
        final CompanyDeleteResponse response = companyService.deleteCompany((company.getSymbol()));

        //Assert
        assertEquals(1, response.getDeletedCompanyCount());
        verify(companyRepository, times(1)).deleteBySymbol(company.getSymbol());
    }

    @Test
    void DeleteCompany_ShouldNotSucceed_GivenThat_CompanyDoesNotExists() {
        //Arrange
        when(companyRepository.existsBySymbol(company.getSymbol())).thenReturn(false);

        //Act
        final CompanyDeleteResponse response = companyService.deleteCompany((company.getSymbol()));

        //Assert
        assertEquals(0, response.getDeletedCompanyCount());
        verify(companyRepository, times(0)).deleteBySymbol(company.getSymbol());
    }

    @Test
    void GetAllCompanies_ShouldSucceed_GivenThat_ThereAreCompanies() {
        //Arrange
        when(companyRepository.findAll()).thenReturn(activeCompanyList);

        //Act
        List<CompanyDto> response = companyService.getAllCompanies();

        //Assert
        assertEquals(LIST_SIZE, response.size());
        for (int i = 0; i < response.size(); i++) {
            assertEquals(activeCompanyList.get(i).getSymbol(), response.get(i).getSymbol());
            assertEquals(activeCompanyList.get(i).getActive(), response.get(i).getActive());
            assertEquals(activeCompanyList.get(i).getCompanyName(), response.get(i).getCompanyName());
            assertEquals(activeCompanyList.get(i).getStockPrice(), response.get(i).getStockPrice());
            assertEquals(activeCompanyList.get(i).getId(), response.get(i).getId());
        }
        verify(companyRepository, times(1)).findAll();
    }

    @Test
    void GetAllCompanies_ShouldNotSucceed_GivenThat_ThereAreNotCompanies() {
        //Arrange
        when(companyRepository.findAll()).thenReturn(new ArrayList<Company>());

        //Act
        List<CompanyDto> response = companyService.getAllCompanies();

        //Assert
        assertEquals(0, response.size());
        verify(companyRepository, times(1)).findAll();
    }

    @Test
    void GetCompaniesByStatus_ShouldSucceed_GivenThat_ThereAreCompaniesWithRequestedStatus() {
        //Arrange
        final boolean status = true;
        when(companyRepository.findByStatus(status)).thenReturn(companyList);

        //Act
        List<CompanyDto> response = companyService.getCompaniesByStatus(status);

        //Assert
        assertEquals(LIST_SIZE, response.size());
        for (int i = 0; i < response.size(); i++) {
            assertEquals(companyList.get(i).getSymbol(), response.get(i).getSymbol());
            assertEquals(companyList.get(i).getActive(), response.get(i).getActive());
            assertEquals(companyList.get(i).getCompanyName(), response.get(i).getCompanyName());
            assertEquals(companyList.get(i).getStockPrice(), response.get(i).getStockPrice());
            assertEquals(companyList.get(i).getId(), response.get(i).getId());
        }
        verify(companyRepository, times(1)).findByStatus(status);
    }

    @Test
    void GetCompaniesByStatus_ShouldNotSucceed_GivenThat_ThereAreNotCompaniesWithRequestedStatus() {
        //Arrange
        final boolean status = false;
        when(companyRepository.findByStatus(status)).thenReturn(new ArrayList<>());

        //Act
        List<CompanyDto> response = companyService.getCompaniesByStatus(status);

        //Assert
        assertEquals(0, response.size());
        verify(companyRepository, times(1)).findByStatus(status);
    }
}
