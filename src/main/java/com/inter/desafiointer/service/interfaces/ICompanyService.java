package com.inter.desafiointer.service.interfaces;

import com.inter.desafiointer.model.dto.CompanyDto;
import com.inter.desafiointer.model.request.CompanyRequest;
import com.inter.desafiointer.model.request.UpdateCompanyStatusRequest;
import com.inter.desafiointer.model.request.UpdateCompanyStockPriceRequest;
import com.inter.desafiointer.model.response.CompanyDeleteResponse;

import java.util.List;

public interface ICompanyService {
    CompanyDto createCompany(CompanyRequest request);

    List<CompanyDto> createCompanies(List<CompanyRequest> requestList);

    CompanyDto updateCompanyStockPrice(UpdateCompanyStockPriceRequest request);

    CompanyDto updateCompanyStatus(UpdateCompanyStatusRequest request);

    CompanyDeleteResponse deleteCompany(String symbol);

    List<CompanyDto> getAllCompanies();

    List<CompanyDto> getCompaniesByStatus(Boolean status);
}
