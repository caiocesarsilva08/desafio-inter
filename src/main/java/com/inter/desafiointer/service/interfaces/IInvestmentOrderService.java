package com.inter.desafiointer.service.interfaces;

import com.inter.desafiointer.model.request.UserInvestmentRequest;
import com.inter.desafiointer.model.request.DetailedInvestmentOrderRequest;
import com.inter.desafiointer.model.response.DetailedCompanyInvestmentOrderResponse;
import com.inter.desafiointer.model.response.DetailedInvestmentOrderResponse;

import java.util.List;

public interface IInvestmentOrderService {
    DetailedInvestmentOrderResponse createDetailedOrder(DetailedInvestmentOrderRequest request);
    List<DetailedCompanyInvestmentOrderResponse> getAllDetailedOrders();
    DetailedInvestmentOrderResponse makeInvestment(UserInvestmentRequest request);
}
