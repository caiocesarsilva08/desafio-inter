package com.inter.desafiointer.service;

import com.inter.desafiointer.model.converter.InvestmentOrderConverter;
import com.inter.desafiointer.model.dto.InvestmentOrderItemDto;
import com.inter.desafiointer.model.entity.Company;
import com.inter.desafiointer.model.entity.InvestmentOrder;
import com.inter.desafiointer.model.entity.InvestmentOrderItem;
import com.inter.desafiointer.model.request.UserInvestmentRequest;
import com.inter.desafiointer.model.request.DetailedInvestmentOrderRequest;
import com.inter.desafiointer.model.request.InvestmentOrderItemRequest;
import com.inter.desafiointer.model.response.CompanyOrderResponse;
import com.inter.desafiointer.model.response.DetailedCompanyInvestmentOrderResponse;
import com.inter.desafiointer.model.response.DetailedInvestmentOrderResponse;
import com.inter.desafiointer.repository.CompanyRepository;
import com.inter.desafiointer.repository.InvestmentOrderItemRepository;
import com.inter.desafiointer.repository.InvestmentOrderRepository;
import com.inter.desafiointer.service.interfaces.IInvestmentOrderService;
import com.inter.desafiointer.utility.CpfUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class InvestmentOrderService implements IInvestmentOrderService {
    @Autowired
    private InvestmentOrderRepository investmentOrderRepository;

    @Autowired
    private InvestmentOrderItemRepository investmentOrderItemRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private InvestmentOrderConverter investmentOrderConverter;

    @Override
    public DetailedInvestmentOrderResponse createDetailedOrder(DetailedInvestmentOrderRequest request) {
        InvestmentOrder orderToBeCreated = new InvestmentOrder(request.getId(), request.getCpf(), request.getOrderTotal(), request.getChangeOrder(), request.getOrderPayment());

        if (validateOrderCpf(request.getCpf()) && validateCompaniesStatus(request)) {
            InvestmentOrder orderCreated = investmentOrderRepository.save(orderToBeCreated);
            if (orderCreated != null) {
                return investmentOrderConverter.toResponse(orderCreated, convertInvestmentOrderItemRequestToDto(orderCreated, request.getOrderItems()));
            }
        }
        return null;
    }

    @Override
    public List<DetailedCompanyInvestmentOrderResponse> getAllDetailedOrders() {
        try {
            List<InvestmentOrder> investmentOrders = investmentOrderRepository.findAll();
            if(investmentOrders == null || investmentOrders.isEmpty()) {
                return new ArrayList<>();
            }
            List<InvestmentOrderItem> investmentOrderItems = investmentOrderItemRepository.findAll();

            if (investmentOrderItems == null || investmentOrderItems.isEmpty()) {
                return new ArrayList<>();
            } else {
                List<DetailedCompanyInvestmentOrderResponse> companyOrderResponses = new ArrayList<>();
                investmentOrders.forEach(order -> {
                    calculateCompanyTotals(companyOrderResponses, order, investmentOrderItems);
                });
                return companyOrderResponses;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public DetailedInvestmentOrderResponse makeInvestment(UserInvestmentRequest request) {
        try {
            if(validateOrderCpf(request.getCpf())) {
                List<Company> companies = companyRepository.findByStatus(true);

                BigDecimal smallestStockPrice = companies.stream().map(Company::getStockPrice).min(BigDecimal::compareTo).get();

                InvestmentOrder investmentOrder = new InvestmentOrder();
                investmentOrder.setCpf(request.getCpf());
                investmentOrder.setChangeOrder(BigDecimal.ZERO);
                investmentOrder.setOrderPayment(request.getInvestedAmount());
                investmentOrder.setOrderTotal(BigDecimal.ZERO);
                InvestmentOrder orderCreated = investmentOrderRepository.save(investmentOrder);
                List<InvestmentOrderItem> investmentOrderItems = knapsack(companies.size(), request.getCompanyNumber(), request.getInvestedAmount(), companies, orderCreated, smallestStockPrice);

                investmentOrderItemRepository.saveAll(investmentOrderItems);

                calculateOrderTotals(orderCreated, investmentOrderItems);

                List<InvestmentOrderItemDto> investmentOrderItemDto = convertInvestmentOrderItemToDto(orderCreated, investmentOrderItems);
                return investmentOrderConverter.toResponse(orderCreated, investmentOrderItemDto);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void calculateOrderTotals(InvestmentOrder orderCreated, List<InvestmentOrderItem> investmentOrderItems) {
        BigDecimal orderTotal = BigDecimal.ZERO;
        BigDecimal orderPayment = orderCreated.getOrderPayment();
        BigDecimal changeOrder = BigDecimal.ZERO;

        for (InvestmentOrderItem investmentOrderItem : investmentOrderItems) {
            orderTotal = orderTotal.add(investmentOrderItem.getCompany().getStockPrice().multiply(BigDecimal.valueOf(investmentOrderItem.getStockQuantity())));
        }

        orderPayment = orderCreated.getOrderPayment();
        changeOrder = orderPayment.subtract(orderTotal);

        orderCreated.setOrderTotal(orderTotal);
        orderCreated.setOrderPayment(orderPayment);
        orderCreated.setChangeOrder(changeOrder);

        investmentOrderRepository.save(orderCreated);
    }

    //TODO: fix the algorithm, he is buying more shares than the maximum amount set in the order
    public List<InvestmentOrderItem> knapsack(Integer index, Integer maximumNumber, BigDecimal desiredValue, List<Company> companies, InvestmentOrder orderCreated, BigDecimal smallestStockPrice) {
        final Integer numberOfCompanies = companies.size();

        if (numberOfCompanies == 0 || desiredValue == BigDecimal.ZERO || index == 0) {
            return new ArrayList<>();
        }

        if (companies.get(index - 1).getStockPrice().compareTo(desiredValue) > 0) {
            return knapsack(index - 1, maximumNumber, desiredValue, companies, orderCreated, smallestStockPrice);
        } else {
            List<InvestmentOrderItem> withCurrentCompany = knapsack(index - 1, maximumNumber, desiredValue.subtract(companies.get(index - 1).getStockPrice()), companies, orderCreated, smallestStockPrice);
            List<InvestmentOrderItem> withoutCurrentCompany = knapsack(index - 1, maximumNumber, desiredValue, companies, orderCreated, smallestStockPrice);
            InvestmentOrderItem investmentOrderItem = new InvestmentOrderItem();
            investmentOrderItem.setCompany(companies.get(index - 1));
            investmentOrderItem.setStockQuantity(1);
            investmentOrderItem.setInvestmentOrder(orderCreated);

            if (withCurrentCompany.size() > withoutCurrentCompany.size()) {
                if (withCurrentCompany.size() < maximumNumber) {
                    withCurrentCompany.add(investmentOrderItem);
                }
                return withCurrentCompany;
            } else {
                if (withoutCurrentCompany.size() < maximumNumber) {
                    withoutCurrentCompany.add(investmentOrderItem);
                }
                return withoutCurrentCompany;
            }
        }
    }

    private boolean validateCompaniesStatus(DetailedInvestmentOrderRequest request) {
        AtomicBoolean validOrder = new AtomicBoolean(true);
        if (!request.getOrderItems().isEmpty()) {
            request.getOrderItems().forEach(item -> {
                Optional<Company> company = companyRepository.findById(item.getCompanyId());
                if (!company.isPresent() || !company.get().getActive()) {
                    validOrder.set(false);
                }
            });
        } else {
            validOrder.set(false);
        }
        return validOrder.get();
    }

    private boolean validateOrderCpf(String cpf) {
        return CpfUtil.isValidCPF(cpf);
    }

    private List<InvestmentOrderItemDto> convertInvestmentOrderItemRequestToDto(InvestmentOrder orderCreated, List<InvestmentOrderItemRequest> orderItems) {
        List<InvestmentOrderItemDto> orderItemDtoList = new ArrayList<>();
        orderItems.forEach(item -> {
            Optional<Company> company = companyRepository.findById(item.getCompanyId());
            InvestmentOrderItem orderItemToBeCreated = new InvestmentOrderItem();
            orderItemToBeCreated.setInvestmentOrder(orderCreated);
            orderItemToBeCreated.setStockQuantity(item.getStockQuantity());
            orderItemToBeCreated.setCompany(company.get());
            InvestmentOrderItem orderItemCreated = investmentOrderItemRepository.save(orderItemToBeCreated);
            orderItemDtoList.add(new InvestmentOrderItemDto(orderItemCreated.getOrderItemId(), orderItemCreated.getStockQuantity(), orderCreated.getOrderId(), orderItemCreated.getCompany().getId()));
        });
        return orderItemDtoList;
    }

    private List<InvestmentOrderItemDto> convertInvestmentOrderItemToDto(InvestmentOrder orderCreated, List<InvestmentOrderItem> orderItems) {
        List<InvestmentOrderItemDto> orderItemDtoList = new ArrayList<>();
        orderItems.forEach(item -> {
            Optional<Company> company = companyRepository.findById(item.getCompany().getId());
            InvestmentOrderItem orderItemToBeCreated = new InvestmentOrderItem();
            orderItemToBeCreated.setInvestmentOrder(orderCreated);
            orderItemToBeCreated.setStockQuantity(item.getStockQuantity());
            orderItemToBeCreated.setCompany(company.get());
            InvestmentOrderItem orderItemCreated = investmentOrderItemRepository.save(orderItemToBeCreated);
            orderItemDtoList.add(new InvestmentOrderItemDto(orderItemCreated.getOrderItemId(), orderItemCreated.getStockQuantity(), orderCreated.getOrderId(), orderItemCreated.getCompany().getId()));
        });
        return orderItemDtoList;
    }

    public void calculateCompanyTotals(List<DetailedCompanyInvestmentOrderResponse> companyOrderResponses, InvestmentOrder order, List<InvestmentOrderItem> investmentOrderItems) {
        List<InvestmentOrderItem> orderItems = new ArrayList<>();
        investmentOrderItems.forEach(item -> {
            if (item.getInvestmentOrder().getOrderId().equals(order.getOrderId())) {
                orderItems.add(item);
            }
        });
        List<CompanyOrderResponse> companyOrderResponsesList = new ArrayList<>();
        orderItems.forEach(item -> {
            Optional<CompanyOrderResponse> companyOrderResponse = companyOrderResponsesList.stream().filter(company -> company.getSymbol().equals(item.getCompany().getSymbol())).findFirst();
            if (companyOrderResponse.isPresent()) {
                companyOrderResponse.get().setCompanyTotal(companyOrderResponse.get().getCompanyTotal().add(BigDecimal.valueOf(item.getStockQuantity()).multiply(item.getCompany().getStockPrice())));
            } else {
                CompanyOrderResponse companyOrderRes = new CompanyOrderResponse();
                companyOrderRes.setStockQuantity(item.getStockQuantity());
                companyOrderRes.setSymbol(item.getCompany().getSymbol());
                companyOrderRes.setCompanyTotal(BigDecimal.valueOf(item.getStockQuantity()).multiply(item.getCompany().getStockPrice()));
                companyOrderResponsesList.add(companyOrderRes);
            }
        });

        DetailedCompanyInvestmentOrderResponse obj = new DetailedCompanyInvestmentOrderResponse();
        obj.setId(order.getOrderId());
        obj.setCpf(order.getCpf());
        obj.setOrderTotal(order.getOrderTotal());
        obj.setChangeOrder(order.getChangeOrder());
        obj.setOrderPayment(order.getOrderPayment());
        obj.setCompanyTotals(companyOrderResponsesList);
        obj.setOrderItems(getOrderItemsForOrder(order.getOrderId(), investmentOrderItems));

        companyOrderResponses.add(obj);
    }

    private List<InvestmentOrderItemDto> getOrderItemsForOrder(Long orderId, List<InvestmentOrderItem> investmentOrderItemList) {
        List<InvestmentOrderItem> orderItems = new ArrayList<>();
        investmentOrderItemList.forEach(item -> {
            if (item.getInvestmentOrder().getOrderId().equals(orderId)) {
                orderItems.add(item);
            }
        });
        List<InvestmentOrderItemDto> orderItemDtoList = new ArrayList<>();
        orderItems.forEach(item -> {
            orderItemDtoList.add(new InvestmentOrderItemDto(item.getOrderItemId(), item.getStockQuantity(), item.getInvestmentOrder().getOrderId(), item.getCompany().getId()));
        });
        return orderItemDtoList;
    }
}
