package com.inter.desafiointer.service;

import com.inter.desafiointer.model.converter.CompanyConverter;
import com.inter.desafiointer.model.dto.CompanyDto;
import com.inter.desafiointer.model.entity.Company;
import com.inter.desafiointer.model.request.CompanyRequest;
import com.inter.desafiointer.model.request.UpdateCompanyStatusRequest;
import com.inter.desafiointer.model.request.UpdateCompanyStockPriceRequest;
import com.inter.desafiointer.model.response.CompanyDeleteResponse;
import com.inter.desafiointer.repository.CompanyRepository;
import com.inter.desafiointer.service.interfaces.ICompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyService implements ICompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyConverter companyConverter;

    @Override
    public CompanyDto createCompany(CompanyRequest request) {
        try {
            if (!companyRepository.existsBySymbol((request.getSymbol()))) {
                final Company objSaved = companyRepository.save(companyConverter.toEntity(request));
                return companyConverter.toDto(objSaved);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<CompanyDto> createCompanies(List<CompanyRequest> requestList) {
        List<CompanyDto> companyDtoList = new ArrayList<CompanyDto>();
        try {
            requestList.forEach(request -> {
                CompanyDto obj = createCompany(request);
                if (obj != null) {
                    companyDtoList.add(obj);
                }
            });

            return companyDtoList;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    @Transactional
    public CompanyDto updateCompanyStockPrice(UpdateCompanyStockPriceRequest request) {
        try {
            if (companyRepository.existsBySymbol(request.getSymbol())) {
                companyRepository.updateStockPriceBySymbol(request.getSymbol(), request.getStockPrice());
                final Company objUpdated = companyRepository.findBySymbol(request.getSymbol());
                return companyConverter.toDto(objUpdated);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    @Transactional
    public CompanyDto updateCompanyStatus(UpdateCompanyStatusRequest request) {
        try {
            if (companyRepository.existsBySymbol(request.getSymbol())) {
                companyRepository.updateStatusBySymbol(request.getSymbol(), request.getActive());
                final Company objUpdated = companyRepository.findBySymbol(request.getSymbol());
                return companyConverter.toDto(objUpdated);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    @Transactional
    public CompanyDeleteResponse deleteCompany(String symbol) {
        try {
            if (companyRepository.existsBySymbol(symbol)) {
                companyRepository.deleteBySymbol(symbol);
                return CompanyDeleteResponse.builder().deletedCompanyCount(1).build();
            } else {
                return CompanyDeleteResponse.builder().deletedCompanyCount(0).build();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<CompanyDto> getAllCompanies() {
        try {
            List<Company> companies = companyRepository.findAll();
            if (companies.isEmpty()) {
                return new ArrayList<CompanyDto>();
            } else {
                return companyConverter.toDto(companies);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<CompanyDto> getCompaniesByStatus(Boolean status) {
        try {
            List<Company> companies = companyRepository.findByStatus(status);
            if (companies == null || companies.isEmpty()) {
                return new ArrayList<CompanyDto>();
            } else {
                return companyConverter.toDto(companies);
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
