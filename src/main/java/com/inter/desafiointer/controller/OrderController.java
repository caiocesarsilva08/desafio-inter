package com.inter.desafiointer.controller;

import com.inter.desafiointer.model.request.UserInvestmentRequest;
import com.inter.desafiointer.model.request.DetailedInvestmentOrderRequest;
import com.inter.desafiointer.model.response.DetailedCompanyInvestmentOrderResponse;
import com.inter.desafiointer.model.response.DetailedInvestmentOrderResponse;
import com.inter.desafiointer.service.InvestmentOrderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderController {
    @Autowired
    private InvestmentOrderService orderService;

    @ApiOperation(value = "Creates an investment with its order items")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Returns the created detailed investment"),
            @ApiResponse(code = 400, message = "Wrong payload"),
            @ApiResponse(code = 500, message = "Exception thrown"),
    })
    @PostMapping("/orders/detailed")
    public ResponseEntity<DetailedInvestmentOrderResponse> createDetailedOrder(@RequestBody DetailedInvestmentOrderRequest request) {
        DetailedInvestmentOrderResponse response = orderService.createDetailedOrder(request);
        if(response == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
    }

    @ApiOperation(value = "Get the list of all orders with its orders items, including the total per company in the order")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Returns the detailed order list created successfully"),
            @ApiResponse(code = 404, message = "Orders not found"),
            @ApiResponse(code = 500, message = "Exception thrown"),
    })
    @GetMapping("/orders/detailed")
    public ResponseEntity<List<DetailedCompanyInvestmentOrderResponse>> getAllDetailedOrders() {
        List<DetailedCompanyInvestmentOrderResponse> response = orderService.getAllDetailedOrders();
        if(response.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Makes the most efficient investment possible for the user, given its conditions")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Returns the investment successfully saved"),
            @ApiResponse(code = 400, message = "Wrong payload"),
            @ApiResponse(code = 500, message = "Exception thrown"),
    })
    @PostMapping("/orders/invest")
    public ResponseEntity<DetailedInvestmentOrderResponse> makeInvestment(@RequestBody UserInvestmentRequest request) {
        DetailedInvestmentOrderResponse response = orderService.makeInvestment(request);
        if(response == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }
    }
}
