package com.inter.desafiointer.controller;

import com.inter.desafiointer.model.dto.CompanyDto;
import com.inter.desafiointer.model.request.CompanyRequest;
import com.inter.desafiointer.model.request.UpdateCompanyStatusRequest;
import com.inter.desafiointer.model.request.UpdateCompanyStockPriceRequest;
import com.inter.desafiointer.model.response.CompanyDeleteResponse;
import com.inter.desafiointer.service.CompanyService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @ApiOperation(value = "Creates a company")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Returns the company created successfully"),
            @ApiResponse(code = 400, message = "Wrong request payload"),
            @ApiResponse(code = 500, message = "Exception thrown"),
    })
    @PostMapping("/companies")
    public ResponseEntity<CompanyDto> createCompany(@RequestBody CompanyRequest request) {
        CompanyDto companyDto = companyService.createCompany(request);
        if (companyDto == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(companyDto, HttpStatus.CREATED);
        }
    }

    @ApiOperation(value = "Creates a list of companies")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Returns the companies list created successfully"),
            @ApiResponse(code = 400, message = "Wrong request payload"),
            @ApiResponse(code = 500, message = "Exception thrown"),
    })
    @PostMapping("/companies/batch")
    public ResponseEntity<List<CompanyDto>> createCompanies(@RequestBody List<CompanyRequest> requestList) {
        List<CompanyDto> companyDtoList = companyService.createCompanies(requestList);
        if (companyDtoList.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(companyDtoList, HttpStatus.CREATED);
        }

    }

    @ApiOperation(value = "Updates the stock price of a company")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Returns the company updated successfully"),
            @ApiResponse(code = 400, message = "Wrong request payload"),
            @ApiResponse(code = 404, message = "Company not found"),
            @ApiResponse(code = 500, message = "Exception thrown"),
    })
    @PostMapping("/companies/stock-price")
    public ResponseEntity<CompanyDto> updateCompanyStockPrice(@RequestBody UpdateCompanyStockPriceRequest request) {
        CompanyDto companyDto = companyService.updateCompanyStockPrice(request);

        if (companyDto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(companyDto, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Updates the status of a company")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Returns the company updated successfully"),
            @ApiResponse(code = 400, message = "Wrong request payload"),
            @ApiResponse(code = 404, message = "Company not found"),
            @ApiResponse(code = 500, message = "Exception thrown"),
    })
    @PostMapping("/companies/status")
    public ResponseEntity<CompanyDto> updateCompanyStatus(@RequestBody UpdateCompanyStatusRequest request) {
        CompanyDto companyDto = companyService.updateCompanyStatus(request);

        if (companyDto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(companyDto, HttpStatus.CREATED);
        }
    }

    @ApiOperation(value = "Deletes a company")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Returns the number of rows deleted"),
            @ApiResponse(code = 404, message = "Company not found"),
            @ApiResponse(code = 500, message = "Exception thrown"),
    })
    @DeleteMapping("/companies/{symbol}")
    public ResponseEntity<CompanyDeleteResponse> deleteCompany(@ApiParam(value = "Company symbol") @PathVariable("symbol") String symbol) {
        CompanyDeleteResponse companyDeleteResponse = companyService.deleteCompany(symbol);

        if (companyDeleteResponse.getDeletedCompanyCount() == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(companyDeleteResponse, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Get all companies")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Returns the companies"),
            @ApiResponse(code = 404, message = "Companies not found"),
            @ApiResponse(code = 500, message = "Exception thrown"),
    })
    @GetMapping("/companies")
    public ResponseEntity<List<CompanyDto>> getAllCompanies() {
        List<CompanyDto> companies = companyService.getAllCompanies();

        if (companies.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(companies, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "Get all companies based on its status")
    @ApiParam(value = "Company status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Returns the companies with the requested status"),
            @ApiResponse(code = 404, message = "Companies with the requested status not found"),
            @ApiResponse(code = 500, message = "Exception thrown"),
    })
    @GetMapping("/companies/{status}")
    public ResponseEntity<List<CompanyDto>> getAllCompaniesByStatus(@ApiParam(value = "Company status") @PathVariable("status") Boolean status) {
        List<CompanyDto> companies = companyService.getCompaniesByStatus(status);

        if (companies.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(companies, HttpStatus.OK);
        }
    }

}
