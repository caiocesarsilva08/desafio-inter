package com.inter.desafiointer.model.dto;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto implements Serializable {
    private Long id;
    private String cpf;
    private BigDecimal orderTotal;
    private BigDecimal changeOrder;
    private BigDecimal orderPayment;
}
