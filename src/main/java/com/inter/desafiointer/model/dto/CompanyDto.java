package com.inter.desafiointer.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Company data object")
public class CompanyDto {
    @ApiModelProperty(value = "Company id")
    private Long id;

    @ApiModelProperty(value = "Company symbol")
    private String symbol;

    @ApiModelProperty(value = "Company name")
    private String companyName;

    @ApiModelProperty(value = "Company stock price")
    private BigDecimal stockPrice;

    @ApiModelProperty(value = "Company status")
    private Boolean active;
}
