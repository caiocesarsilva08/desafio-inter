package com.inter.desafiointer.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Investment order item data object")
public class InvestmentOrderItemDto implements Serializable {
    @ApiModelProperty(value = "Order Item ID")
    private Long orderItemId;

    @ApiModelProperty(value = "Stock quantity")
    private Integer stockQuantity;

    @ApiModelProperty(value = "Order ID")
    private Long investmentOrderId;

    @ApiModelProperty(value = "Company ID")
    private Long companyId;
}