package com.inter.desafiointer.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvestmentOrderItem {
    @Id
    @GeneratedValue
    private Long orderItemId;

    @Column
    private Integer stockQuantity;

    @ManyToOne
    private InvestmentOrder investmentOrder;

    @ManyToOne
    private Company company;
}
