package com.inter.desafiointer.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvestmentOrder {
    @Id
    @GeneratedValue
    private Long orderId;

    @Column
    private String cpf;

    @Column
    private BigDecimal orderTotal;

    @Column
    private BigDecimal changeOrder;
    
    @Column
    private BigDecimal orderPayment;
}
