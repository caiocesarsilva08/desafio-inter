package com.inter.desafiointer.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Company {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String symbol;

    @Column
    private String companyName;

    @Column
    private BigDecimal stockPrice;

    @Column
    private Boolean active;

}
