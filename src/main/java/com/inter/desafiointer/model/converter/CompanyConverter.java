package com.inter.desafiointer.model.converter;

import com.inter.desafiointer.model.dto.CompanyDto;
import com.inter.desafiointer.model.entity.Company;
import com.inter.desafiointer.model.request.CompanyRequest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CompanyConverter {
    public CompanyDto toDto(Company entity) {
        if(entity == null) {
            return null;
        } else {
            return CompanyDto.builder()
                    .id(entity.getId())
                    .companyName(entity.getCompanyName())
                    .stockPrice(entity.getStockPrice())
                    .symbol(entity.getSymbol())
                    .active(entity.getActive())
                    .build();
        }
    }

    public List<CompanyDto> toDto(List<Company> companies) {
        if(companies == null || companies.isEmpty()) {
            return null;
        } else {
            List<CompanyDto> convertedCompanies = new ArrayList<CompanyDto>();
            companies.forEach(company -> {
                convertedCompanies.add(this.toDto(company));
            });
            return convertedCompanies;
        }
    }

    public Company toEntity(CompanyRequest request) {
        if(request == null) {
            return null;
        } else {
            return Company.builder()
                    .companyName(request.getCompanyName())
                    .stockPrice(request.getStockPrice())
                    .symbol(request.getSymbol())
                    .active(request.getActive())
                    .build();
        }
    }
}
