package com.inter.desafiointer.model.converter;

import com.inter.desafiointer.model.dto.InvestmentOrderItemDto;
import com.inter.desafiointer.model.dto.OrderDto;
import com.inter.desafiointer.model.entity.InvestmentOrder;
import com.inter.desafiointer.model.entity.InvestmentOrderItem;
import com.inter.desafiointer.model.request.OrderRequest;
import com.inter.desafiointer.model.response.DetailedInvestmentOrderResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InvestmentOrderConverter {
    public OrderDto toDto(InvestmentOrder entity) {
        if(entity == null) {
            return null;
        } else {
            return OrderDto.builder()
                    .id(entity.getOrderId())
                    .changeOrder(entity.getChangeOrder())
                    .cpf(entity.getCpf())
                    .orderTotal(entity.getOrderTotal())
                    .orderPayment(entity.getOrderPayment())
                    .build();
        }
    }

    public InvestmentOrder toEntity(OrderRequest request) {
        if(request == null) {
            return null;
        } else {
            return InvestmentOrder.builder()
                    .changeOrder(request.getChangeOrder())
                    .cpf(request.getCpf())
                    .orderTotal(request.getOrderTotal())
                    .orderPayment(request.getOrderPayment())
                    .build();
        }
    }

    public DetailedInvestmentOrderResponse toResponse(InvestmentOrder orderCreated, List<InvestmentOrderItemDto> orderItems) {
        if(orderCreated == null || orderItems.isEmpty()) {
            return null;
        } else {
            DetailedInvestmentOrderResponse response = new DetailedInvestmentOrderResponse();
            response.setId(orderCreated.getOrderId());
            response.setCpf(orderCreated.getCpf());
            response.setOrderTotal(orderCreated.getOrderTotal());
            response.setChangeOrder(orderCreated.getChangeOrder());
            response.setOrderPayment(orderCreated.getOrderPayment());
            response.setOrderItems(orderItems);
            return response;
        }

    }

    public List<DetailedInvestmentOrderResponse> toResponse(List<InvestmentOrder> investmentOrders, List<InvestmentOrderItem> investmentOrderItems) {
        if (investmentOrders.isEmpty() || investmentOrderItems.isEmpty()) {
            return null;
        } else {
            List<DetailedInvestmentOrderResponse> response = new ArrayList<>();
            for (InvestmentOrder order : investmentOrders) {
                List<InvestmentOrderItemDto> orderItems = new ArrayList<>();
                for (InvestmentOrderItem item : investmentOrderItems) {
                    if (item.getInvestmentOrder().getOrderId() == order.getOrderId()) {
                        orderItems.add(InvestmentOrderItemDto.builder()
                                .companyId(item.getCompany().getId())
                                .investmentOrderId(item.getInvestmentOrder().getOrderId())
                                .orderItemId(item.getOrderItemId())
                                .stockQuantity(item.getStockQuantity())
                                .build());
                    }
                }
                response.add(toResponse(order, orderItems));
            }
            return response;
        }
    }

    public List<InvestmentOrderItemDto> toDto(List<InvestmentOrderItem> investmentOrderItems) {
        if(investmentOrderItems.isEmpty()) {
            return null;
        } else {
            List<InvestmentOrderItemDto> response = new ArrayList<>();
            for (InvestmentOrderItem item : investmentOrderItems) {
                response.add(InvestmentOrderItemDto.builder()
                        .companyId(item.getCompany().getId())
                        .investmentOrderId(item.getInvestmentOrder().getOrderId())
                        .orderItemId(item.getOrderItemId())
                        .stockQuantity(item.getStockQuantity())
                        .build());
            }
            return response;
        }
    }
}