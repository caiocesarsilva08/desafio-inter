package com.inter.desafiointer.model.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Detailed investment with company totals response")
public class DetailedCompanyInvestmentOrderResponse extends DetailedInvestmentOrderResponse implements Serializable {
    @ApiModelProperty(value = "Company totals")
    List<CompanyOrderResponse> companyTotals;
}