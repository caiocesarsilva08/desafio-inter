package com.inter.desafiointer.model.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CompanyOrderResponse implements Serializable {
    private String symbol;
    private BigDecimal companyTotal;
    private Integer stockQuantity;
}
