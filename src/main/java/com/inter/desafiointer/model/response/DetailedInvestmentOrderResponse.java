package com.inter.desafiointer.model.response;

import com.inter.desafiointer.model.dto.InvestmentOrderItemDto;
import com.inter.desafiointer.model.dto.OrderDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Detailed investment response")
public class DetailedInvestmentOrderResponse extends OrderDto implements Serializable {
    @ApiModelProperty(value = "Items of an investment order")
    List<InvestmentOrderItemDto> orderItems;
}