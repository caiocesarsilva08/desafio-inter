package com.inter.desafiointer.model.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class CompanyDeleteResponse implements Serializable {
    private Integer deletedCompanyCount;
}
