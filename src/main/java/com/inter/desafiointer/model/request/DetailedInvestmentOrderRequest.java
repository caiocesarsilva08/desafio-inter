package com.inter.desafiointer.model.request;

import com.inter.desafiointer.model.dto.OrderDto;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Detailed investment order request model")
public class DetailedInvestmentOrderRequest extends OrderDto implements Serializable {
    List<InvestmentOrderItemRequest> orderItems;
}
