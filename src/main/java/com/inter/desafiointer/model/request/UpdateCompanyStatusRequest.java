package com.inter.desafiointer.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@ApiModel(value = "Company status update request model")
public class UpdateCompanyStatusRequest implements Serializable {
    @ApiModelProperty(value = "Company symbol")
    String symbol;

    @ApiModelProperty(value = "New company status")
    Boolean active;
}