package com.inter.desafiointer.model.request;

import com.inter.desafiointer.model.dto.OrderDto;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
public class OrderRequest extends OrderDto implements Serializable {
}
