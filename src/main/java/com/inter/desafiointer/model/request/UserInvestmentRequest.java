package com.inter.desafiointer.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@ApiModel(value = "User investment request object")
public class UserInvestmentRequest implements Serializable {
    @ApiModelProperty(value = "User CPF")
    String cpf;

    @ApiModelProperty(value = "Numbers of companies that the user intends to invest")
    Integer companyNumber;

    @ApiModelProperty(value = "The amount paid by the user to the investment")
    BigDecimal investedAmount;
}
