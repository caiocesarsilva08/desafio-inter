package com.inter.desafiointer.model.request;

import lombok.Data;
import java.io.Serializable;

@Data
public class InvestmentOrderItemRequest implements Serializable {
    private Integer stockQuantity;

    private Long companyId;
}
