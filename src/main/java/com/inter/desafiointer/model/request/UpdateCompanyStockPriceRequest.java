package com.inter.desafiointer.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@ApiModel(value = "Company stock price update request model")
public class UpdateCompanyStockPriceRequest implements Serializable {
    @ApiModelProperty(value = "Company symbol")
    String symbol;

    @ApiModelProperty(value = "New company stock price")
    BigDecimal stockPrice;
}
