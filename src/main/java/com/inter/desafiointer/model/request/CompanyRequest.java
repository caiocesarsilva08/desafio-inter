package com.inter.desafiointer.model.request;

import com.inter.desafiointer.model.dto.CompanyDto;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Company request model")
public class CompanyRequest extends CompanyDto implements Serializable {
}
