package com.inter.desafiointer.repository;

import com.inter.desafiointer.model.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
    public Boolean existsBySymbol(String symbol);

    @Modifying
    @Query("update Company c set c.stockPrice = :stockPrice where c.symbol = :symbol")
    public Integer updateStockPriceBySymbol(@Param("symbol") String symbol, @Param("stockPrice") BigDecimal stockPrice);

    @Query("select c from Company c where c.symbol = :symbol")
    public Company findBySymbol(@Param("symbol") String symbol);

    @Modifying
    @Query("update Company c set c.active = :status where c.symbol = :symbol")
    public Integer updateStatusBySymbol(@Param("symbol") String symbol, @Param("status") Boolean status);

    @Modifying
    @Query("delete from Company c where c.symbol = :symbol")
    public void deleteBySymbol(@Param("symbol") String symbol);

    @Query("select c from Company c where c.active = :status")
    public List<Company> findByStatus(@Param("status") Boolean status);
}
