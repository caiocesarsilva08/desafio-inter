package com.inter.desafiointer.repository;

import com.inter.desafiointer.model.entity.InvestmentOrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestmentOrderItemRepository extends JpaRepository<InvestmentOrderItem, Long> {
}
