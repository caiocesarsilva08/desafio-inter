package com.inter.desafiointer.repository;

import com.inter.desafiointer.model.entity.InvestmentOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestmentOrderRepository extends JpaRepository<InvestmentOrder, Long> {
}
