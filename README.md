# Desafio Inter

## Description

This is the code challenge made for the Inter Bank. It is an application made using Java/Maven/Spring Boot (version 2.6.4) and SQLite database, with its structure focused on an API service. In this project, you will be able to create/view/change companies with their respective actions, as well as make investments in an automated way.

## Installation
To clone and run this application, you'll need
- [Git](https://git-scm.com)
- [Postman (recommended)](https://www.postman.com/)
- [JDK (version 17.0.02)](https://jdk.java.net/17/)
- [SQLite](https://www.sqlite.org/download.html)
- [Intellij IDEA](https://www.jetbrains.com/idea/download/#section=windows)

```bash
# Clone this repository
$ git clone https://gitlab.com/caiocesarsilva08/desafio-inter.git
```

Then, you can use the Intellij IDEA interface to build and run the project.

To build the project, go to the top of the Intellij interface and click in the Build button:<br>
<img src="pictures/build-project.png"><br>
If everything went ok, you should be able to see this in the Build tab:<br>
<img src="pictures/build-window.png"><br>

## Usage
To run the main project, you need to go the main directory, then, inside the java folder, click with the right-button in the DesafioInterApplication, then click to Run it:<br>
<img src="pictures/main-project.png"><br>

If everything went ok, you should be able to see this message in the Run terminal:

```bash
INFO 8824 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
```

To see all the documented endpoints, access:
```bash
http://localhost:8080/swagger-ui.html
```

To run the unit tests you can click with the right-button in the test application, located inside the source folder, in the test directory:<br>
<img src="pictures/test-project-location.png"><br>
After clicking with the right-button in the test application, select the "Run <NameOfTheFileThatYouClicked>" option. <br>
Then, all the project tests should be running, you can see the results in the Run window:<br>
<img src="pictures/run-tests-window.png"><br>

Every route in this application is documented using Swagger and Postman, you can import the collection located in the JSON file
```bash
desafio.postman_collection.json
```

It is using Collection V2.1 format.

I'm not using an external database, so every time I launch the application, I need to populate the database by calling the batch company creation route:

```bash
http://localhost:8080/companies/batch
```

In the request body you can use whatever values you want, but it needs to be in the structure below:
```bash
[
    {
        "companyName": "Inter",
        "symbol": "BIDI11",
        "stockPrice": 66.51,
        "active": true
    },
    {
        "companyName": "Magazine Luiza",
        "symbol": "MGLU3",
        "stockPrice": 18.80,
        "active": true
    },
    {
        "companyName": "Sulamérica",
        "symbol": "SULA11",
        "stockPrice": 28.26,
        "active": false
    },
    {
        "companyName": "Engie",
        "symbol": "EGIE3",
        "stockPrice": 38.30,
        "active": true
    },
    {
        "companyName": "CVC",
        "symbol": "CVCB3",
        "stockPrice": 20.87,
        "active": false
    },
    {
        "companyName": "Renner",
        "symbol": "LREN3",
        "stockPrice": 36.95,
        "active": true
    },
    {
        "companyName": "Marisa",
        "symbol": "AMAR3",
        "stockPrice": 6.30,
        "active": false
    }
]
```
Then, you're ready to execute whatever endpoint you want.

## Support
If you have any problems with this project, you can email me to the address: [caiocesarsilva08@gmail.com](mailto:caiocesarsilva08@gmail.com)


## Project status/notes
The project is not yet fully completed. The method to make investments in an automated way is not working fully, the investment amount is exceeding user payment amount, I am working on it. I also need to create an exception handling and logging system, which I intend to do in the future. Another very important detail that I need to implement is a connection to an external database, so that we don't have to fill it every time we run the application, in addition to caching to optimize performance. Also, there are some parts of the code that I still need to unit test.